"""
Class to store parameter and write them later.
Written by:Baltasar
on: 6.7.2019

Usage:

    >> from Parameterstorage import ParameterStorer 
    >> ps = ParameterStorer()
    >> conductivity = ps("conductivity", 1e-3)
    >> ps.write_csv(path="./Results/", filename="parameter.csv")

"""
import os
import datetime

class ParameterStorer(object):
    """Class to store parameters of a simulation.
    Calling the class adds a parameter to the class and stores it.

    Usage (example):
    >> params = Parameter()
    >> epsilon = params("epsilon", epsilon) # returns epsilon and stores it in Parameter class
    
    If one wants to use a parameter later, simply do:

    >> params.epsilon # parameter given to the class are set as attributes.

    """

    def __init__(self):
        """
        Create Parameter object.
        """

        "This is used later to store parameter names explictly."
        self._csv_atts = {}

    def csv_att(self, var_name, var_val=None, csv_name=None):
        """ Set var_name as attribute with value var_val and include attribute in csv-output.
        Args:
            var_name: name of the attribute
            var_val: value of the attribute, default: None
            csv_name: name in csv file. if none: use var_name, default: None
        Returns:
            var_val: value to which the parameter has been set.
        """
        var_name = var_name.replace(",", "-")
        csv_name = csv_name.replace(",", "-") if csv_name else csv_name
        setattr(self, var_name, var_val)
        csv_name = csv_name if csv_name else var_name
        self._csv_atts[csv_name] = var_val
        return var_val

    def csv_headline(self, prefix=""):
        """ Return alphabetic ordered csv-string of attribute names marked for csv output.
        Args:
            prefix: prefix will put in front of the attribute names. Default: ""

        Example:
            Assume attributes names marked for csv are "dt", "dx" and "nx".
            Calling make_csv_headline(prefix='leader') returns:
                "leaderdt, leaderdx, leadernx"
        """
        names_with_prefix = [prefix + attname for attname in sorted(self._csv_atts.keys())]
        csv_headline_string = ",".join(names_with_prefix)
        return csv_headline_string

    def csv_values(self):
        """ Return values of attributes marked for csv output in alphabetic order of the attribute names.
        """
        att_vals = []
        for attrname in sorted(self._csv_atts.keys()):
            var_as_string = str(self._csv_atts[attrname]).replace(",", "-")
            att_vals.append(remove_whitespace(var_as_string))
        att_vals_csv_string = ",".join(att_vals)
        return att_vals_csv_string

    def _create_csv_file(self, path, filename, overwrite=False):
        """ Create file filename at path.
        Args:
            path: path to file
            filename: name of file
            overwrite: if file should be overwritten if it exists.
        Returns:
            path to file (i.e. path/filename)
        """
        
        if file_extension(filename) == "":
            filename = filename + ".csv"
        elif file_extension(filename) == "csv":
            pass
        else:
            raise ValueError(
                "Only .csv filenames are allowed. Extension given is {}.".format(file_extension(filename))
            )
        path_to_file = "/".join([path, filename])
        if overwrite or not os.path.exists(path +"/" + filename):
            with open(path_to_file, "w+") as f:
                f.write("# created on {} by Baltasar\n".format(get_date_and_time()))
        else: 
            raise FileExistsError("File {} exists. cannot be created.".format(path_to_file))
        self.csv_path = path_to_file
        return path_to_file

    def _write_csv_headline(self, path, filename):
        csv_file = self.csv_path
        append_to_file(csv_file, self.csv_headline())

    def _write_csv_vals(self, path, filename):
        csv_file = self.csv_path
        append_to_file(csv_file, self.csv_values())

    def write_csv(self, path, filename="parameter.csv", overwrite=False):
        """ Write csv parameter to file.
        Args:
            path: path to folder containing the file
            filename: filename, default: 'parameter.csv'
            overwrite: if file should be overwritten if it exists.
        """
        path = path + "/" if not path.endswith("/") else path
        self._create_csv_file(path, filename, overwrite)
        self._write_csv_headline(path, filename)
        self._write_csv_vals(path, filename)

    def __call__(self, parameter_name, parameter_value, csv_name=None):
        """ Set var_name as attribute with value var_val and include attribute in csv-output.
        Args:
            var_name: name of the attribute
            var_val: value of the attribute, default: None
            csv_name: name in csv file. if none: use var_name, default: None
        Returns:
            var_val: value to which the parameter has been set.
        """
        return self.csv_att(parameter_name, parameter_value, csv_name)

    def remove_parameter(self, parametername):
        """ Deletes a parameter stored in the tracker by name.
        """
        try:
            self._csv_atts.pop(parametername)
        except KeyError:
            pass



""" helper functions """
def remove_whitespace(string):
    """Removes whitespace in string and returns string.
    Args:
        - string: string with potentially whitespace in it
    Returns:
        - string without whitespace.
    """

    while " " in string:
        string = string.replace(" ", "")
    return string

def file_extension(filename):
    """Returns filename extension that is present in filename.
    Args:
        - filename: name of the file
    Returns:
        - filename extension
    """
    splitted = filename.split(".")
    # hidden files are not supported
    if filename.startswith("."):
         raise ValueError(
             "'{}' is a hidden file. Hidden files are not supported.".format()
         )
    if len(splitted) == 1:
        return ""
    else:
        return splitted[-1]


def append_to_file(path_to_file, string_to_append):
    """Append string_to_append to file path_of_file.
    Args:
        path_to_file: path to file
        string_to_append: string to append to file
    """
    with open(path_to_file, "a") as f:
        f.write(string_to_append + '\n')

def get_date_and_time():
    """returns a string with year-month-day_hourhminute, e.g. 20170517_09h00"""
    now = datetime.datetime.now()
    date = str(now.date())
    hour = str(now.hour)
    minute = str(now.minute)
    if len(minute) == 1:
        minute = "0" + minute
    if len(hour) == 1:
        hour = "0" + hour
    return date + "_" + hour + "h" + minute

