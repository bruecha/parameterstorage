import sys

sys.path.insert(0, "../")
import ParameterStorage


storer = ParameterStorage.ParameterStorer()
storer.read_from_csv("./test_input/Parameters.csv", format="columnwise")
storer.write_csv(".", overwrite=True, format="columnwise")
