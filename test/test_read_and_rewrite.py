from ParameterStorage import ParameterStorer


def main():
    storer = ParameterStorer()
    storer.read_from_csv("./test_input/Parameters-linewise.csv")
    storer.write_csv("./test_input", "Parameters-reread-linewise.csv", overwrite=True)
    storer.write_csv(
        "./test_input",
        "Parameters-reread-columnwise.csv",
        overwrite=True,
        format="columnwise",
    )


if __name__ == "__main__":
    main()
