"""
Class to store parameter and write them later.
Written by:Baltasar
on: 6.7.2019
"""
import pandas as pd


class ParameterStorage(object):
    """This class loads the pandas dframe and provides some methods
    to help.
    """

    def __init__(self, path, convert=True):
        """
        Args:
            - path: path to pandas file
            - convert: if dtypes should be converted to best possible,
              default: True
        """
        dframe = pd.read_pickle(path)
        if convert:
            dframe = dframe.convert_dtypes()
            dframe = dframe.apply(pd.to_numeric, errors="ignore")
        self.dframe = dframe

    def get(self, column_name, value):
        """Returns sub dframe where the value in column_name is equal
        to value.
        Args:
            - column_name: the column name
            - value: the value
        Returns:
            - dframe: pandas dataframe
        Notes:
            if the value argument is a string, it has to be a string
            in a string: "'string value'"
        """
        return self.dframe.query(f"`{column_name}`== {value}")

    def columns(self):
        """Return column names"""
        return ", ".join((column_name for column_name in self.dframe.columns))

    def add_columns(self, colname1, colname2, name=None):
        """Adds values in column 1 and column2 and add them to the dframe
        Args:
            - colname1 : columnname1
            - colname2 : colname2
            - name: Name of the new column. If None: use colname1+colname2,
              default: None
        Returns:
            - the dataframe with the column attached
        """
        name = str(name) if name else str(colname1) + str(colname2)
        if name not in self.dframe.columns:
            self.dframe[name] = self.dframe[colname1] + self.dframe[colname2]

        else:
            raise ValueError(f"Column with name {name} already exists.")

        return self.dframe

    def filter(self, column, value):
        """Remove database entries where column is not value.
        Args:
            - column: which column to use
            - value: which value is okay
        Note:
            This operation automatically overwrite dframe with the new
            dframe.
        """
        mask = self.dframe[column] == value
        self.dframe = self.dframe[mask]

    def drop(self, column, condition):
        """Drops rows where condition is met in row.

        Args:
            - column: which column to take
            - condition: which condition to fulfill in order to be removed; callable
        """
        remove = self.dframe[column].apply(condition)
        self.dframe = self.dframe[~remove]

    def drop_test(self, column="sim_identifier", test_value="_Test"):
        """Remove rows where the values in column are test_value.
        Args:
            - column: which volumn has `test_value` as test indicator, default:
                sim_identifier
            - test_value: if test_value in 'column': drop the entire row, default: _Test
        Notes:
            - all the string values are lowered.
        """

        def condition(x):
            return test_value in x

        self.drop(column, condition)

    def save_pickle(self, path):
        """Saves the dframe under 'path'
        Args:
            - path: the path to save the dframe
        """
        path = path + ".pkl" if not path.endswith(".pkl") else path
        self.dframe.to_pickle(path)
