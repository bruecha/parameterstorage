"""
Class to store parameter and write them later.
Written by:Baltasar
on: 6.7.2019
"""
import os
import datetime


class ParameterStorer(object):
    """Class to store parameters of a simulation.
    Calling the class adds a parameter to the class and stores it.

    Usage (example):
    >> params = Parameter()
    >> # return epsilon and store it in Parameter class:
    >> epsilon = params("epsilon", epsilon)

    If one wants to use a parameter later, simply do:

    >> params.epsilon # parameter given to the class are set as attributes.

    """

    def __init__(self):
        """
        Create Parameter object.
        Notes for formats:
            linewise means: one parameter per line
            columnwise means: one parameter per column
        """

        "This is used later to store parameter names explictly."
        self._csv_atts = {}
        self._formats = {"per-line": "linewise", "per-col": "columnwise"}

    def csv_att(self, var_name, var_val=None, csv_name=None):
        """Set var_name as attribute with value var_val and include attribute in csv-output.
        Args:
            var_name: name of the attribute
            var_val: value of the attribute, default: None
            csv_name: name in csv file. if none: use var_name, default: None
        Returns:
            var_val: value to which the parameter has been set.
        """
        var_name = var_name.replace(",", "-")
        csv_name = csv_name.replace(",", "-") if csv_name else var_name
        setattr(self, var_name, var_val)
        self._csv_atts[csv_name] = var_val
        return var_val

    @staticmethod
    def _val_to_string(val):
        """Remove comma and type conversion induced spaces and new line characters"""
        if not isinstance(val, str):
            # replace space that comes from type conversion
            val = str(val).replace(" ", "")
        # space will be delimiter so there cannot be spaces anywhere.
        else:
            val = val.replace(" ", "_")
        return val

    def show_current_storage(self):
        """returns as string what is stored atm."""
        ret_val = "\n".join(
            (
                " : ".join([str(key), str(value)])
                for key, value in self._csv_atts.items()
            )
        )
        return ret_val

    def csv_headline(self, prefix=""):
        """Return alphabetic ordered csv-string of attribute names marked for csv output.
        Args:
            prefix: prefix will put in front of the attribute names. Default: ""

        Example:
            Assume attributes names marked for csv are "dt", "dx" and "nx".
            Calling make_csv_headline(prefix='leader') returns:
                "leaderdt, leaderdx, leadernx"
        """
        names_with_prefix = [
            self._val_to_string(prefix + attname)
            for attname in sorted(self._csv_atts.keys())
        ]
        csv_headline_string = ",".join(names_with_prefix)
        return csv_headline_string

    def csv_values(self):
        """
        Return values of attributes marked for csv output in
        alphabetic order of the attribute names.
        """
        att_vals = []
        for attrname in sorted(self._csv_atts.keys()):
            val = self._csv_atts[attrname]
            att_vals.append(self._val_to_string(val))
        att_vals_csv_string = ",".join(att_vals)
        return att_vals_csv_string

    def _write_csv_linewise(self):
        """Write the csv one entry per column"""
        content = []
        for attrname in sorted(self._csv_atts.keys()):
            name = attrname.replace(" ", "_")
            value = self._val_to_string(self._csv_atts[attrname])
            content.append(" ".join([name, value]))
        content = "\n".join(content)
        append_to_file(self.csv_path, content)

    def _create_csv_file(self, path, filename, overwrite=False):
        """Create file filename at path.
        Args:
            path: path to file
            filename: name of file
            overwrite: if file should be overwritten if it exists.
        Returns:
            path to file (i.e. path/filename)
        """

        if file_extension(filename) == "":
            filename = filename + ".csv"
        elif file_extension(filename) == "csv":
            pass
        else:
            raise ValueError(
                "Only .csv filenames are allowed. Extension given is {}.".format(
                    file_extension(filename)
                )
            )
        path_to_file = "/".join([path, filename])
        if overwrite or not os.path.exists(path + "/" + filename):
            with open(path_to_file, "w+") as f:
                f.write("# created on {} by Baltasar\n".format(get_date_and_time()))
        else:
            raise FileExistsError(
                "File {} exists. cannot be created.".format(path_to_file)
            )
        self.csv_path = path_to_file
        return path_to_file

    def _write_csv_headline(self, path, filename):
        csv_file = self.csv_path
        append_to_file(csv_file, self.csv_headline())

    def _write_csv_vals(self, path, filename):
        csv_file = self.csv_path
        append_to_file(csv_file, self.csv_values())

    def write_csv(self, path, filename="parameter.csv", overwrite=False, format=None):
        """Write csv parameter to file.
        Args:
            path: path to folder containing the file
            filename: filename, default: 'parameter.csv'
            overwrite: if file should be overwritten if it exists.
            format: if None, defaults to 'linewise': one simulation per line,
              if 'columnwise' one simulation per column, raises error else.
        """

        path = path + "/" if not path.endswith("/") else path
        self._create_csv_file(path, filename, overwrite)
        if format in [None, self._formats["per-line"]]:
            self._write_csv_linewise()
        elif format == self._formats["per-col"]:
            self._write_csv_headline(path, filename)
            self._write_csv_vals(path, filename)
        else:
            raise NotImplementedError(
                " ".join(
                    [
                        f"Format '{format}' not implemented.",
                        "Allowed are:\n",
                        "\n".join(self._formats.values()),
                    ]
                )
            )

    def __call__(self, parameter_name, parameter_value, csv_name=None):
        """Set var_name as attribute with value var_val and include attribute in csv-output.
        Args:
            var_name: name of the attribute
            var_val: value of the attribute, default: None
            csv_name: name in csv file. if none: use var_name, default: None
        Returns:
            var_val: value to which the parameter has been set.
        """
        return self.csv_att(parameter_name, parameter_value, csv_name)

    def change_parameter(self, parametername, new_value):
        """Change the value of parameter with parametername to new value.
        Args:
            - parametername: the parameter name
            - new_value: the new value of the parameter
        """
        self._csv_atts[parametername] = new_value

    def remove_parameter(self, parametername):
        """Deletes a parameter stored in the tracker by name."""
        try:
            self._csv_atts.pop(parametername)
        except KeyError:
            pass

    def read_from_csv(self, path_to_file, comments="#", format="linewise"):
        """Reads the parameters stored in path_to_file
        and stores them as attributes of this parameterclass instance.
        Args:
            - path_to_file: the path to the file.
            - comments: lines starting with comments are skipped.
            - format: how the file is formated. Only 'linewise' implemented atm.

        Notes:
            - The format in the csv has to be:


                parametername,parametername,parametername
                parametervalue,parametervalue,parametervalue
            - If a paramter cannot be set, this will be printed
            and skipped.
        """
        if format == "linewise":
            self._read_linewise(path_to_file, comments)
        elif format == "columnwise":
            self._read_columnwise(path_to_file, comments)
        else:
            raise NotImplementedError(
                " ".join(
                    [
                        f"Format '{format}' not implemented.",
                        "Allowed are:\n",
                        "\n".join(self._formats.values()),
                    ]
                )
            )

    def _read_columnwise(self, path_to_file, comments):
        """Read data written in the columnwise format
        Args:
            - path_to_file: the filepath
            - comments: the sign for comments
        """
        with open(path_to_file) as f:
            lines = [i for i in f.readlines() if not i.startswith(comments)]
        csv_names, vals = [], []
        for line in lines:
            csv_name, val = line.split(" ")[0:2]
            csv_names.append(csv_name)
            vals.append(val.replace("\n", ""))
        self._store_from_csv_names_and_vals(csv_names, vals)

    def _read_linewise(self, path_to_file, comments):
        """Read data written in the linewise format
        Args:
            - path_to_file: the filepath
            - comments: the sign for comments
        """
        with open(path_to_file) as f:
            lines = [i.strip() for i in f.readlines() if not i.startswith(comments)]
        # one line has the structure: csv_name csv_value
        csv_names, vals = [], []
        for line in lines:
            try:
                name, val = line.split(" ")
            except ValueError:
                (name,) = line.split(" ")
                val = ""
            csv_names.append(name)
            vals.append(val)

        self._store_from_csv_names_and_vals(csv_names, vals)

    def _store_from_csv_names_and_vals(self, csv_names, vals):
        if not len(csv_names) == len(vals):
            raise ValueError("The csv-file given is inconsistent.")
        for csv_name, val in zip(csv_names, vals):
            parameter_name = remove_text_inside_brackets(
                csv_name.replace(" ", "").replace("-", "_"), brackets="()[]{}<>"
            )
            try:
                val = float(val)
                val = int(val) if val.is_integer() else val
            except ValueError:
                val = val  # keep as string
            self.csv_att(parameter_name, val, csv_name)

    def get_value(self, var_name):
        """Returns value of 'var_name'
        Args:
            - var_name: the variable name.
        """
        return self._csv_atts[var_name]


""" helper functions """


def _make_attribute_compatible(s):
    """Makes string s python attribute compatible
    -> removes spaces, replaces '-' with '_'
    Args:
        - s: the string
    Returns:
        - string without spaces and minuses
    """
    return s.replace(" ", "").replace("-", "_")


def file_extension(filename):
    """Returns filename extension that is present in filename.
    Args:
        - filename: name of the file
    Returns:
        - filename extension
    """
    splitted = filename.split(".")
    # hidden files are not supported
    if filename.startswith("."):
        raise ValueError(
            "'{}' is a hidden file. Hidden files are not supported.".format(filename)
        )
    if len(splitted) == 1:
        return ""
    else:
        return splitted[-1]


def append_to_file(path_to_file, string_to_append):
    """Append string_to_append to file path_of_file.
    Args:
        path_to_file: path to file
        string_to_append: string to append to file
    """
    with open(path_to_file, "a") as f:
        f.write(string_to_append + "\n")


def remove_text_inside_brackets(text, brackets="()[]"):
    """Remove all text between brackets.
    Args:
        - text: the text
        - brackets: opening and closing brackets as pairs
    Returns:
        the text without brackets

    Note: Taken form: https://stackoverflow.com/a/14603508
    """
    count = [0] * (len(brackets) // 2)  # count open/close brackets
    saved_chars = []
    for character in text:
        for i, b in enumerate(brackets):
            if character == b:  # found bracket
                kind, is_close = divmod(i, 2)
                count[kind] += (-1) ** is_close  # `+1`: open, `-1`: close
                if count[kind] < 0:  # unbalanced bracket
                    count[kind] = 0  # keep it
                else:  # found bracket to remove
                    break
        else:  # character is not a [balanced] bracket
            if not any(count):  # outside brackets
                saved_chars.append(character)
    return "".join(saved_chars)


def get_date_and_time():
    """returns a string with year-month-day_hourhminute, e.g. 20170517_09h00"""
    now = datetime.datetime.now()
    date = str(now.date())
    hour = str(now.hour)
    minute = str(now.minute)
    if len(minute) == 1:
        minute = "0" + minute
    if len(hour) == 1:
        hour = "0" + hour
    return date + "_" + hour + "h" + minute
