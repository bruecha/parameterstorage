from ParameterStorage import ParameterGatherer


def gather(
    sim_result_folder,
    output_file,
    csv_filename,
    exclude_pattern,
    parameter_order,
    unique_identifier=None,
):
    """Gathers parameters saved under sim_result_folder with csv_filename pattern.
    Certain parameter files can be excluded by giving exclude_pattern (e.g. files
    in which path there is 'test' by exclude_pattern='test').

    Args:
        - sim_result_folder: Folder under which all the rest will be
        - output_file: File where to store the results. Append to file if exists
        - csv_filename: Pattern how to identify the parameter files
        - exclude_pattern: Files having this pattern will be excluded
        - parameter_order: if the csvs are columnwise or linewise.
        - unique_identifier: parameter name that is unique along the simulations.
            When this entr is already present, don't use the new one.

    """
    gatherer = ParameterGatherer(sim_result_folder, output_file)
    gatherer.gather_parameter(
        must_contain=csv_filename,
        must_not_contain=exclude_pattern,
        parameter_order=parameter_order,
        unique_identifier=unique_identifier,
    )
