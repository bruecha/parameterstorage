def convert_to_iterable(string, sep, start=1, stop=-1, type=str):
    """Returns the string representing an iterable as iterable seperated by
    sep and tries to convert the values to type.
    Args:
        - string: the string to be converted. strip will be called on string,
        before anything else counts -> adapt start
        - sep: separator between iterable entries
        - start: index of first entry of iterable, default: 1, if string start
        with first entry: take 0
        - stop: index of last value of iterable, default: -1, if string does not end
        with closing brakets or similar, take None
        - type: callable, try to cast to type.

    Returns:
        - list of the entries saved in the string

    """
    # empty string represents no value given
    if string == "":
        return []
    val = string.strip()
    val = val[start:stop]

    iterable = []
    for entry in val.split(sep):
        try:
            entry = type(entry)
        except ValueError:
            entry = entry
        iterable.append(entry)
    return iterable
