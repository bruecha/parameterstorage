"""
"""
import pathlib
import os
import re
import time

import pandas as pd


__all__ = ["ParameterGatherer"]


class ParameterGatherer(object):
    def __init__(self, sim_results_folder, output_file):
        """Initialise the parameter Gatherer.
        Args:
            - sim_results_folder: folder where simulation experiments
            are stored
            - output_file: Path to the pandas array at which to append
            or to a new picke file. Example: 'results.pkl'.


        Notes:
            The sim_results_folder is meant to be the root folder
            of the simulation results.
            For example if you have an experiment Folder:
                YYYY-MM-DD_experiment_name
            And there a subfolder:
                results/
            sim_results_folder should be .../YYYY-MM-DD_experiment_name/results/
            the result file pattern file be given later, also possibly with sub
            folders.
        """
        self._res_folder_path = sim_results_folder
        self._output_file = output_file

    def gather_parameter(
        self,
        must_contain="",
        search_subfolder=True,
        must_not_contain="",
        converterfunctions=None,
        print_info=True,
        parameter_order="linewise",
        unique_identifier=None,
    ):
        """Gather parameter stored in csv files in sim_results_folder and saves
        them in output_file.
        Args:
            - must_contain: Text that must be present in file paths, default: ""
            - search_subfolder: if continue search in subfolders, default: True
            - must_not_contain: text that must not be in the filepath: default: ""
            - converterfunctions: dictionary with functions to use for conversion for
            pandas. Dictionary keys are panda column names, values are callables to
            be applied on the values. Default: None. (automatic conversion from pandas),
            see pandas.read_csv.
            - print_info: if information about what's going on should be printed.
            - parameter_order: if the parameter are ordere linewise or columnwise
            - unique_identifier: parameter name that is unique along the simulations.
                When this entr is already present, don't use the new one.

        Returns:
            - The data frame containing all the parameters.

        Note:
            - there are assumptions made about the csv file structure:
                - comments are given by '#',
                - the seperator is: ',' (not ', ')
                - there is no column that gives the index of the row.

        """
        extension = "csv"  # necessary because _merged_dframes uses pd.read_csv
        converterfunctions = (
            dict() if converterfunctions is None else converterfunctions
        )
        print_info and print("Working on {}".format(self._res_folder_path))
        files = _list_of_files(
            self._res_folder_path,
            extension,
            must_contain,
            search_subfolder,
            must_not_contain,
        )
        dframe = _open_or_create_dframe(self._output_file)
        t = time.time()
        print_info and print("Getting & Merging files.")
        dframe = _merged_dframes(
            files, dframe, converterfunctions, unique_identifier, parameter_order
        )
        self.dframe = dframe
        print_info and print(f"Time merging: {time.time() - t} seconds.")

        # make output file folder if it doesn't exist
        output_file_folder = pathlib.Path(self._output_file).parent
        os.makedirs(output_file_folder, exist_ok=True)
        save_pd_dframe_pickle(dframe, self._output_file)
        print_info and print(f"Saved to {self._output_file}.")
        return dframe


def _list_of_files(path, extension, containsTxt="", subFolders=False, excludeText=""):
    r"""Recursive function to find all files of an extension type in
    a folder (and optionally in all subfolders too)

    Args:
        - path: Base directory to find files
        - extension: File extension to find.  e.g. 'txt'.
            Regular expression. Or  'ls\d' to match ls1, ls2, ls3 etc
        - containsTxt: List of Strings, only finds file if it contains this text.
            Ignore if '' (or blank)
        - subFolders: Bool.  If True, find files in all subfolders under path.
            If False, only searches files in the specified folder
        - excludeText: Text string or list of strings.  Ignore if ''. Will exclude the
        if text string is in path.

     Returns:
         - iterator over filenames

    Note: stolen from https://stackoverflow.com/a/49647980
    """
    if isinstance(containsTxt, str):  # if a string and not in a list
        containsTxt = [containsTxt]
    if isinstance(excludeText, str):
        excludeText = [excludeText]

    myregexobj = re.compile(
        r"\." + extension + "$"
    )  # Makes sure the file extension is at the end and is preceded by a .

    try:  # Trapping a OSError or FileNotFoundError:  File permissions problem I believe
        for entry in os.scandir(path):
            if entry.is_file() and myregexobj.search(entry.path):  #

                bools = [
                    True
                    for txt in containsTxt
                    if txt in entry.path
                    and (
                        excludeText == [""]
                        or all([text not in entry.path for text in excludeText])
                    )
                ]

                # each containsTxt is in the path
                if len(bools) == len(containsTxt):
                    yield entry.path

            elif (
                entry.is_dir() and subFolders
            ):  # if its a directory, then repeat process as a nested function
                yield from _list_of_files(
                    entry.path, extension, containsTxt, subFolders, excludeText
                )
    except OSError as ose:
        print("Cannot access " + path + ". Probably a permissions error ", ose)
    except FileNotFoundError as fnf:
        print(path + " not found ", fnf)


def _open_or_create_dframe(path_to_file):
    """Open dframe with path_to_file. If the file
    does not exist: return empy pd. dataframe.
    File must be of type pkl.
    Args:
        - path_to_file: path to file
    Returns:
        - pandas dataframe
    Raises:
        - ValueError if path_to_file does not point to pickel file.
    """

    if not path_to_file.endswith(".pkl"):
        msg = "path_to_file must end with .pkl but is {}.".format(path_to_file)
        raise ValueError(msg)
    print("reading {}".format(path_to_file))
    try:
        dframe = pd.read_pickle(path_to_file)
        print("Finish reading pickle file.")
    except FileNotFoundError:
        dframe = pd.DataFrame([])
        print("File {} not found. Created Dataframe.".format(path_to_file))
    return dframe


def _merged_dframes(
    filenames,
    orig_dataframe,
    converter_functions,
    unique_identifier,
    parameter_order="linewise",
):
    """Merge dataframes that are store in filenames to orig_dataframe.
    Some assumptions are made about the csv file structure.

    """

    def read_dframes(
        filenames,
        converter_functions,
        index_col,
        header,
        parameter_order,
        unique_identifier,
    ):
        if parameter_order == "linewise":
            dframes = [
                pd.read_csv(
                    csv_file,
                    comment="#",
                    sep=" ",
                    skipinitialspace=True,
                    converters=converter_functions,
                    index_col=index_col,
                    header=header,
                    engine="c",
                )
                for csv_file in filenames
            ]

        elif parameter_order == "columnwise":
            dframes = [
                pd.read_csv(
                    csv_file,
                    comment="#",
                    sep=" ",
                    skipinitialspace=True,
                    converters=converter_functions,
                    index_col=index_col,
                    header=header,
                    engine="c",
                ).T
                for csv_file in filenames
            ]

        # make one array, use unique-identifier as index if possible
        dframe_new = pd.concat(dframes, ignore_index=True)
        if unique_identifier:
            dframe_new.set_index(unique_identifier, drop=False, inplace=True)
        return dframe_new

    if all(
        [
            unique_identifier,
            not orig_dataframe.empty,
            unique_identifier not in orig_dataframe.columns,
        ]
    ):
        raise ValueError("Unique identifier not in original dataframe.")
    elif all([unique_identifier, not orig_dataframe.empty]):
        orig_dataframe.set_index(unique_identifier, drop=False, inplace=True)

    if parameter_order == "linewise":
        index_col = False
        header = 0
    elif parameter_order == "columnwise":
        index_col = 0
        header = None
    else:
        raise NotImplementedError(
            f"order {parameter_order} not implemented."
            " Implemented are:"
            " 'linewise', 'columnwise'."
        )
    new_entries = read_dframes(
        filenames,
        converter_functions,
        index_col,
        header,
        parameter_order,
        unique_identifier,
    )

    if not orig_dataframe.empty:
        result_frame = orig_dataframe.combine_first(new_entries)
    else:
        result_frame = new_entries

    return result_frame


def save_pd_dframe_pickle(dframe, path):
    """save pandas dataframe as pickle
    Args:
        - dframe: the frame
        - path: where to save
    """
    dframe.to_pickle(path)
