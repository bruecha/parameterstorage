import ParameterGatherer
from os.path import join as pathjoin

############### converter stuff 

def standard_converters():
    """Return standard converter for me as function dict.
    Args:
        -
    Return:
        - dict of functions for type conversion.

    The dict keys give the column name, the dict values
    have to be a callable that returns the value to use.
    """
    converter_functions = {"num_spirals": num_spirals_reader,
                           'synchron':synchron_converter,
                           'coupling_param_r_assim':int_converter,
                           "drive_N":int_converter,
                           "start_assim":int_converter,
                           "comments":comments_converter,
                           "simid": sim_id_converter}

    return converter_functions

def synchron_converter(x):
    if str(x) == "True":
        return True
    else:
        try:
            return float(str(x))
        except ValueError:
            return int(-1)

def int_converter(x):
    if not float(x).is_integer():
        raise ValueError(f"{x} is not integer. Something is wrong.")
    return int(float(x))

def comments_converter(x):
    return str(x)

def num_spirals_reader(x):
    try:
        return int(float(x))
    except ValueError:
        return str(x)

def sim_id_converter(x):
    while x.endswith("-"):
        x = x[:-1]
    return x

def main(sim_results_folder, output_file,
         parameter_file_contains, search_subfolder,
         must_not_contain, converterfunctions
        ):
    """Gather parameter stored in .csv files and store them in one pandas array.
    Args:
        - sim_results_folder: base folder of the simulation results
        - output_file: path + filename.pkl from the output file. If the
        file eists, will be appended to the file.
        - parameter_file_contains: string that must be in parameter file.
        This is usefull if one has checkpoints with parameter files (e.g. info_cp_1.csv)
        and a parameterfile at the end (e.g. parameters.csv). This is one method
        of choosing the latter only.
        - search_subfolder: if csv data in the subfolders of sim_results_folder should
        be searched as well
        - must_not_contain: csv files that incluse this pattern are excluded, similar purpose
        of parameter_file_contains.
        - converterfunctions: converter functions used for the conversion of csv entries.

    """
    pg = ParameterGatherer.ParameterGatherer(sim_results_folder, output_file)
    pg.gather_parameter(
        parameter_file_contains, search_subfolder, must_not_contain, converterfunctions
    )


if __name__ == "__main__":
    base = "/Users/baltasar_phd/PhD/NumericalExperiments/2019_LowPassfilter/2020-11-05_BothNoNRestingCheckOutsideRangeCheckpointing/Experiments/"
    args = {
        "sim_results_folder" : pathjoin(base, "Results/2020-11-05_different_initials/"),
        'output_file': pathjoin(base, "Results/2020-11-05_different_initials/", "ms-sigmoid", "results.pkl"),
        'parameter_file_contains' : "parameters_",
        'search_subfolder' : True,
        'must_not_contain' : "",
        'converterfunctions':standard_converters(),
    }
    main(**args)



