This contains two closely connected modules:
1) Parametertracker: Import it in simulations, save parameternames and values as csv data

2) The gather_parameter script that makes use of some functions stored in ParameterGathering to unify all the different csv files into one pandas array.

The script 'gather_parameter.py' can be used via python gather_parameter.py. To find out on the usage use python gather_parameter.py --help.
